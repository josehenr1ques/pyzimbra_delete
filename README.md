## pyzimbra_delete

Python wrapper around zmmailbox that helps to mass delete mails in Zimbra Server according to criteria 1000 messages at a time (limitations of zmmailbox)

Disclaimer: Use at your own risk.


```
python pyzimbra_delete.py --help

usage: pyzimbra_delete.py [-h] [--accounts ACCOUNTS] [--all_accounts ALL_ACCOUNTS]
              [--attachment ATTACHMENT] [--larger LARGER] [--after AFTER]
              [--before BEFORE] [--sender SENDER] [--test TEST]
              [--logall LOGALL]

Python wrapper around zmmailbox that helps to mass delete mails in Zimbra
Server according to conditions

optional arguments:
  -h, --help            show this help message and exit
  --accounts ACCOUNTS   Accounts to delete mails (user0@domain.com,user1@...).
  --all_accounts ALL_ACCOUNTS
                        Delete mails in all accounts on server that match
                        condition.
  --attachment ATTACHMENT
                        Filter if mails have attachment.
  --larger LARGER       Filter by size of attachment (#). Larger then...
  --after AFTER         Filter mails after this MM/DD/YYYY. DATE < before.
  --before BEFORE       Filter mails before this MM/DD/YYYY. DATE > after.
  --sender SENDER       Sender to delete mails (abuser@domain.com)
  --test TEST           Must be test=0 to delete mails
  --logall LOGALL       If you want to log stdout to logfile then logall=1
```

On each execution a logfile is created: YYYYMMDD_HHMM_pzd.log

If test option is not specified will run default with test = 1, meaning it won't delete messages. To delete mails test=0 must be passed.

If executed with test=1 or test=0 not specified:

 + No message is deleted
 + If all_accounts=1 then it will only log the first 1000 messages matching criteria for each account before searching for messages in next account
 + To fine tune what messages will be deleted before executing with test=0 you can narrow the search with after, before, attachment and larger options and logall=1 to log all stdout


## Installation

Download pyzimbra_delete.py and run under zimbra user on the mailserver.

```
sudo su zimbra
cd && mkdir pyzimbra_delete && cd pyzimbra_delete
wget https://gitlab.com/josehenr1ques/pyzimbra_delete/-/raw/main/pyzimbra_delete.py
```

## Usage Examples:

- Deleting all messages with an attachment in October 2019 from specific accounts:

``python pyzimbra_delete.py --accounts=jose@domain.com,carla@domain.com --attachment=1 --after=10/01/2019 --before=11/01/2019 --test=0``

- Deleting all messages with an attachment greater than 5MB in October 2019 from specific accounts:

``python pyzimbra_delete.py --accounts=jose@domain.com,carla@domain.com --larger=5 --after=10/01/2019 --before=11/01/2019 --test=0``

- Deleting all messages with an attachment greater than 5MB from specific accounts:

``python pyzimbra_delete.py --accounts=jose@domain.com,carla@domain.com --larger=5 --test=0``

- Deleting all messages before November 2019 from all accounts in the server:

``python pyzimbra_delete.py --all-accounts=1 --before=11/01/2019 --test=0``

- Deleting all messages before November 2019 from all accounts in the server with attachment larger than 5MB and log all stdout to logfile:

``python pyzimbra_delete.py --all-accounts=1 --larger=5 --before=11/01/2019 --logall=1 --test=0``

- Delete all messages from sender@malware.com in jose@domain.com account and log all stdout to logfile:

``python pyzimbra_delete.py --accounts=jose@domain.com --sender=user@malware.com --test=0 --logall=1``


## Support

You can create an issue here, for personalized support, inquiries or help you can reach me at [telegram](https://t.me/josehenr1ques) or by email at jose -> fundamento.dev

