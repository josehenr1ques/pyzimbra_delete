#pyzimbra_delete
#telegram: t.me/josehenr1ques
#email: jose->fundamento.dev
#https://gitlab.com/josehenr1ques/pyzimbra_delete/

import argparse
import sys
import os
import subprocess
import datetime
import logging
import pdb


#LOGGING
today_time = datetime.datetime.now().strftime("%Y%m%d_%H%M")
logging.basicConfig(filename=today_time + '_pzd.log', encoding='utf-8', level=logging.INFO)


#ARGPARSE
option = argparse.ArgumentParser(description='Python wrapper around zmmailbox that helps to mass delete mails in Zimbra Server according to conditions')
option.add_argument("--accounts", default="", help="Accounts to delete mails (user0@domain.com,user1@...).")
option.add_argument("--all_accounts", default=0, type=int, help="Delete mails in all accounts on server that match condition.")
option.add_argument("--attachment", default=0, type=int, help="Filter if mails have attachment.")
option.add_argument("--larger", default=0, type=int, help="Filter by size of attachment (#). Larger then...")
option.add_argument("--after", default="", help="Filter mails after this MM/DD/YYYY. DATE < before.")
option.add_argument("--before", default="", help="Filter mails before this MM/DD/YYYY. DATE > after.")
option.add_argument("--sender", default="", help="Sender to delete mails (abuser@domain.com)")
option.add_argument("--test", default=1, type=int, help="Must be test=0 to delete mails")
option.add_argument("--logall", default=0, type=int, help="If you want to log stdout to logfile then logall=1")

if len(sys.argv) < 2:
    option.print_help()
    sys.exit(1)

options = option.parse_args()
logging.info(options)
#Take vars out of options Namespace
locals().update(vars(options))


#CHECK OPTIONS
#accounts
if len(accounts) > 0 and all_accounts == 0:
    accounts = accounts.split(',')
elif len(accounts) > 0 and all_accounts == 1:
    print('Or specified accounts or all_accounts must be passed as options, not both. Check your options')
    exit()
elif len(accounts) == 0 and all_accounts == 0:
    print('There is no account to process. Set accounts or all_accounts')
    exit()


#all_accounts
if all_accounts == 1:
    accounts = os.popen('zmprov -l gaa').read().split('\n')

#attachment
if attachment == 1:
    attachment = "has:attachment"
else: attachment = ""

#larger
if int(larger) > 0:
    larger = "larger:" + str(larger) + "MB"
else: larger = ""

#before
if len(before) > 0:
    before = "before:" + '"' + before + '"'
else: before = ""

#after
if len(after) > 0:
    after = "after:" + '"' + after + '"'
else: after = ""


#check if before and after are present and validate
if len(before) > 0 and len(after) > 0:
        try:
            before_date = datetime.datetime.strptime(options.before, "%m/%d/%Y")
            after_date = datetime.datetime.strptime(options.after, "%m/%d/%Y")
            if before_date < after_date:
                print("Before date is older than After date, can't continue... Review the options and examples.")
                exit()
        except Exception as p:
            if hasattr(p, 'message'):
                print(p.message)
            else:
                print(p)
            exit()

#sender
if len(sender) > 0:
    sender = "From:" + '"' + sender + '"'
else:
    sender = ""

#test
if test == 1:
    logging.info("Test run. Won't delete messages.")


#conditions
condition = attachment + " " + larger + " " + before + " " + after + " " + sender
condition = condition.replace("  ", " ")


#main_loop_for_account_or_accounts
for account in accounts:
    message_with_condition = 1
    while message_with_condition == 1:
        delete_message_list = []
        total_messages = 0
        message_ids = ''
        print('Searching for messages with condition {0} in account: {1}'.format(condition, account))
        messages_list = subprocess.Popen(['zmmailbox', '-z', '-m', account, 's', '-t', 'message', '-l',  '1000', condition],
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = messages_list.communicate()
        messages_list = stdout.decode("UTF-8").split('\n')
        print(stdout)
        if logall == 1:
            logging.info(stdout)
        if len(messages_list) > 3:
            for message in messages_list:
                message = message.split()
                try:
                    if int(message[1]) and len(message) > 4:
                        delete_message_list.append(message[1])
                except Exception as e:
                    if hasattr(e, 'message'):
                        logging.error(e.message)
                    else:
                        logging.error(e)
                    continue
            print('Found {0} messages matching condition...'.format(len(delete_message_list)))
            total_messages = len(delete_message_list) + total_messages
            message_ids = ','.join(delete_message_list)
            #delete messages if test == 0, must be explicitely pass, else test == 1 and wont delete
            if test == 0:
                delete_messages = subprocess.Popen(['zmmailbox', '-z', '-m', account, 'deleteMessage', message_ids], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                stdout, stderr = delete_messages.communicate()
                print(stdout)
                if logall == 1:
                    logging.info(stdout)
            elif test != 0 and all_accounts == 0:
                pass
            elif test != 0 and all_accounts == 1:
                break
            logging.info("Deleting message_ids: {0} from account: {1}".format(message_ids, account))
        else:
            print('No more messages with condition "{0}" where found for account: {1}'.format(condition, account))
            message_with_condition = 0
            break
        logging.info("{0} messages were deleted from account: {1}".format(total_messages, account))
